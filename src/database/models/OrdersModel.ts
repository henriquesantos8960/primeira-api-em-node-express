import {DataTypes, Model, Optional} from "sequelize";
import {sequelize} from "../../database/sequelize";

interface OrderAttribute {
    orderNumber: number,
    orderDate: string,
    requiredDate: string,
    shippedDate?: string,
    status: string,
    comments?: string,
    customerNumber: number,
};

// Essas interfaces auxiliares são necessárias caso o id da tabela seja de auto incremento
export interface OrderInput extends Optional<OrderAttribute, "orderNumber">{};
export interface OrderOutput extends Required<OrderAttribute>{};

class Order extends Model <OrderAttribute, OrderInput> {
    declare orderNumber: number;
    declare orderDate: string;
    declare requiredDate: string;
    declare shippedDate: string;
    declare status: string;
    declare comments: string;
    declare customerNumber: number;
};

Order.init({
    orderNumber: {type: DataTypes.STRING, primaryKey: true},
    orderDate: {type: DataTypes.STRING},
    requiredDate: {type: DataTypes.STRING},
    shippedDate: {type: DataTypes.STRING},
    status: {type: DataTypes.STRING},
    comments: {type: DataTypes.STRING},
    customerNumber: {type: DataTypes.NUMBER},
}, {
    sequelize,
    modelName: "orders"
});

export default Order;