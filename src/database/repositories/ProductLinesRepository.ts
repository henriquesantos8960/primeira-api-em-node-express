import AppError from "../../utils/AppError";
import model, {ProductLineInput, ProductLineOutput} from "../models/ProductLinesModel";

export const getAll = async (): Promise<ProductLineOutput[]> => {
    return await model.findAll();
};

export const getById = async (id: number): Promise<ProductLineOutput> => {
    const productLine = await model.findByPk(id);

    if (!productLine) {
        throw new AppError("NotFoundError", "Registro não encotrado", 404);
    };

    return productLine;
};

export const create = async (payload: ProductLineInput): Promise<ProductLineOutput> => {
    return await model.create(payload);
};

export const updateById = async (id: number, payload: ProductLineInput): Promise<ProductLineOutput> => {
    const productLine = await model.findByPk(id);

    if (!productLine) {
        throw new Error("Registro não encontrado");
    };

    return await productLine.update(payload);
};

export const deleteById = async (id: number): Promise<void> => {
    const productLine = await model.findByPk(id);

    if (!productLine) {
        throw new Error("Registro não encontrado");
    };

    await productLine.destroy();
};